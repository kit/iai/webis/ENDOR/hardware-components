# ENDOR (ENergy Data of Offices and Residential buildings) Project

## PCB Designs to connect 1x / 8x DS18B20 or DHT22 sensors to a Raspberry Pi SBC

This repository contains the PCB designs in EAGLE format (and as PDF) used in THOR project to connect DS18B20 temperature or DHT22 temperature/relative humidity sensors to Raspberry Pi GPIOs.

Copyright (c) 2017-2020 ENDOR Project / Institute for Automation and Applied Informatics (IAI), Karlsruhe Institute of Technology (KIT)

For license information see LICENSE.txt

### Single Sensor
This design connects a single sensor to GPIO#4 (BCM numbering scheme). The board is designed to attach on a Raspberry Pi just like other shields.

The board uses a pull-up resistor R1=4.7k&#8486;, which works for both sensor types (DS18B20 or DHT22).

![Single Sensor Board on a Raspberry Pi Zero W](img/1x_board_on_RPiZeroW_DS18B20_thumb.jpg "Single Sensor Board on a Raspberry Pi Zero W with DS1820 sensor")

* Design: [1x_Sensor.sch](1x_Sensor/1x_Sensor.sch) ([Preview](1x_Sensor/1x_Sensor_circuit.pdf))
* Board: [1x_Sensor.brd](1x_Sensor/1x_Sensor.brd) ([Preview](1x_Sensor/1x_Sensor_board.pdf))

Since we did not have enough 6 pin box header at hand, the board has holes for 10 pin box header :-)

### 8 Sensors
This design connects up to 8 sensors via RJ45 connectors (e.g. to use existing, unused ethernet cable infrastructure).

The design uses 4.7k&#8486; pull-up resistors (default for short cables and DS18B20). This might have to be adapted depending on connected sensor (DS18B20 or DHT22) and cable length. For further information see our paper.

* Design: [8x_Sensor_RJ45.sch](8x_Sensor_RJ45/8x_Sensor_RJ45.sch) ([Preview](8x_Sensor_RJ45/8x_Sensor_RJ45_circuit.pdf))
* Board: [8x_Sensor_RJ45.brd](8x_Sensor_RJ45/8x_Sensor_RJ45.brd) ([Preview](8x_Sensor_RJ45/8x_Sensor_RJ45_board.pdf)).

Since ethernet cables are usually star-wired, each sensor is connected to a seperate pin and multiple 1-wire busses have to be configured on the SBC. Be aware of reflections when connecting branched cables to a single connector - this might not work if branches are > ~10m in length.   

GPIOs used (BCM numbering scheme, RJ45 connectors from left to right):
1. GPIO#6
2. GPIO#16
3. GPIO#12
4. GPIO#5
5. GPIO#24
6. GPIO#18
7. GPIO#17
8. GPIO#4
